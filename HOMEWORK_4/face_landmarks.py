import cv2
import face_recognition

vc = cv2.VideoCapture(0)

cascade_smile = cv2.CascadeClassifier('./haarcascade_smile.xml')

while True:
	ret, frame = vc.read()
	if ret:
		gray_scale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		smile = cascade_smile.detectMultiScale(gray_scale, 1.3, 20)
		print(smile)
		for x_smile, y_smile, w_smile, h_smile in smile:
			cv2.rectangle(frame, (x_smile, y_smile), (x_smile + w_smile, y_smile + h_smile), (0, 0, 255), 3)
			y = y_smile - 15 if y_smile - 15 > 15 else y_smile + 15
			cv2.putText(frame, 'SMILED :)', (x_smile, y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 3)
		landmarks_dict_list = face_recognition.face_landmarks(frame)
		for landmarks_dict in landmarks_dict_list:
			for name, points in landmarks_dict.items():
				if  name:
					prev_point = points[0]
					for point in points[1:]:
						cv2.line(frame, prev_point, point, (0, 255, 0), 2)
						prev_point = point
					cv2.line(frame, points[0], points[-1], (0, 255, 0), 2)
		cv2.imshow('img', frame)
	if cv2.waitKey(1) & 0xFF == ord('q'):
		break

